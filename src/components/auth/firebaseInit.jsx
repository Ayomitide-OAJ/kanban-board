import Firebase from 'firebase';


// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseInit = Firebase.initializeApp({
  apiKey: "AIzaSyDM6F4Yf_AW69STyMnxHPj_c_n9wajGkMs",
  authDomain: "kanban-board-efc19.firebaseapp.com",
  databaseURL: "https://kanban-board-efc19.firebaseio.com",
  projectId: "kanban-board-efc19",
  storageBucket: "kanban-board-efc19.appspot.com",
  messagingSenderId: "98287716638", 
  appId: "1:98287716638:web:89f1a4f3dd7356ecc9610f",
  measurementId: "G-R8847L6XDG"

});

export default firebaseInit;

